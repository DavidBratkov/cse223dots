/* This project was done in a group.
 * Group Members:
 *   David Bratkov
 *   Logan Buffington
 */

import java.awt.Graphics;
import javax.swing.JPanel;

public class Panelpaint extends JPanel{
	
	int x=0;//The x value of the line
	int y=0;//The y value of the line
	int x2=0; //The height of the line
	int y2=0; //The width of the line
	String newname = "";
	int namex = 0;
	int namey = 0;
	
	public void change(int x1, int y1, int h, int w) {
		x = x1;
		y = y1;
		x2 = h;
		y2 = w;
	}
	
	public void ownerchange(String name, int x, int y) {
		newname = name;
		namex = x+25;
		namey = y+25;
		
		
	}
	
	 public void paint(Graphics graphic){
			int size = 7;
			for(int i=100;i < 550; i += 50){
				for(int j=100;j < 550; j += 50){
					graphic.fillOval(i, j, size, size);
				}
			}
			
			graphic.drawString(newname, namex, namey);
			
			graphic.drawLine(x, y, x2, y2);
	}
}