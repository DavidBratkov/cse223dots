/* This project was done in a group.
 * Group Members:
 *   David Bratkov
 *   Logan Buffington
 */

import java.awt.EventQueue; //All of the imports
import javax.swing.JFrame;
import javax.swing.border.EmptyBorder;
import java.awt.event.MouseEvent;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.SwingConstants;
import javax.swing.JButton;
import javax.swing.JTextField;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;

public class Dots extends JFrame {

	private JPanel contentPane; //main jpanel
	private Panelpaint game; //custom jpanel with a graphic
	private JTextField textField; //place where the user stores the first player's name
	private JTextField textField_1; // place where the user stores the second player's name

	/**
	 * Launch the application.
	 */
	
	//boolean running = true;
	
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					Dots frame = new Dots();
					frame.setVisible(true);
					//Game.main(null);
					
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	boolean turn1 = true;
	boolean validturn = false;
	String name = null;
	int score1 = 0, score2 = 0;
	
	
	public Dots() {
		
		

		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		//setBounds(800, 300, 437, 311);
		setBounds(800, 300, 437, 311);
		contentPane = new JPanel();

		
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(null);
		
		JLabel turn = new JLabel("Turn");
		turn.setBounds(248, 25, 128, 26);
		contentPane.add(turn);
		
		JLabel P1score = new JLabel("Player 1's score");
		P1score.setBounds(400, 25, 102, 26);
		contentPane.add(P1score);
		
		JLabel P2score = new JLabel("Player 2's score");
		P2score.setBounds(500, 25, 102, 26);
		contentPane.add(P2score);
		
		JButton restartbutton = new JButton("Restart");
		restartbutton.setBounds(50, 50, 102, 24);
		contentPane.add(restartbutton);
		
		turn.setVisible(false);
		P1score.setVisible(false);
		P2score.setVisible(false);
		restartbutton.setVisible(false);
		
		JLabel lblNewLabel = new JLabel("Please enter both player's names");
		lblNewLabel.setVerticalTextPosition(SwingConstants.BOTTOM);
		lblNewLabel.setBounds(110, 11, 206, 23);
		contentPane.add(lblNewLabel);
		
		textField = new JTextField();
		textField.setBounds(52, 95, 86, 20);
		contentPane.add(textField);
		textField.setColumns(10);
		
		textField_1 = new JTextField();
		textField_1.setBounds(278, 95, 86, 20);
		contentPane.add(textField_1);
		textField_1.setColumns(10);
		
		JLabel lblNewLabel_1 = new JLabel("Player 1");
		lblNewLabel_1.setBounds(65, 77, 46, 14);
		contentPane.add(lblNewLabel_1);
		
		JLabel lblNewLabel_2 = new JLabel("Player 2");
		lblNewLabel_2.setBounds(293, 77, 46, 14);
		contentPane.add(lblNewLabel_2);
		

		
		JButton continuebutton = new JButton("Continue");
		continuebutton.setBounds(163, 205, 89, 23);
		contentPane.add(continuebutton);
						
		game = new Panelpaint();
		game.setVisible(false);
		game.setLayout(null);
		game.setBorder(new EmptyBorder(5, 5, 5, 5));
		game.setBounds(0, 0, 650, 650);
		contentPane.add(game);
		
		
		continuebutton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent arg0) {
				
				if (!textField.getText().equals("") && !textField_1.getText().equals("")) {
					continuebutton.setVisible(false);
					lblNewLabel.setVisible(false);
					lblNewLabel_1.setVisible(false);
					textField.setVisible(false);
					textField_1.setVisible(false);
					lblNewLabel_2.setVisible(false);
					turn.setText(textField.getText() + "'s Turn");
					//lblNewLabel_2.setText(textField_1.getText() + "'s Turn");
					setBounds(600, 200, 650, 650);
					//contentPane.setBounds(600, 200, 650, 650);
					turn.setBounds(260, 11, 266, 37);
					
					game.setVisible(true);
					game.change(0, 0, 0, 0);
					game.ownerchange("", 0, 0);
					turn.setVisible(true);
					P1score.setVisible(true);
					P2score.setVisible(true);
					restartbutton.setVisible(true);
					
					score1 = 0; //reset p1's score
					P1score.setText(textField.getText().toUpperCase().charAt(0) + ": " + score1);
					
					score2 = 0; //reset p2's score
					P2score.setText(textField_1.getText().toUpperCase().charAt(0) + ": " + score2);
					
					
					//while(running){
						
						name = textField.getText(); //set the first name value to player 1 since they go first	
						turn.setText(name + "'s Turn"); //set the text box at the top to display player 1's turn
						
						//game.paint(game.getGraphics());
						
						box[][] boxfield = new box[8][8];//This initilizes a new array of boxes
						
						for(int i = 0;i < 8;i++) {
							for(int j = 0; j < 8;j++) {
								boxfield[i][j] = new box();//This double for loop assigns each box in the array with a new box
							}
						}
						
						//System.out.println("box 1=" + boxfield[0][0].owner );
				
						game.addMouseListener(new MouseAdapter() {
							@Override
							public void mouseClicked(MouseEvent arg0) {
								System.out.println("x=" + arg0.getX() + "y=" + arg0.getY());
								
								validturn = false; //start off the turn assuming they're gonna frick up
								
								for(int i=0;i < 8;i++) {//This for loop will find which column of boxes the user clicked on
									
									if(((i*50+100) < arg0.getX() && (i*50+150) > arg0.getX()) || (i == 7 && arg0.getX() < 520)) { //this should check if its within the box
										
										for(int j=0;j < 8;j++) {//This for loop will find which row of boxes the user clicked on
											
											if((j*50+100) < arg0.getY() && (j*50+150) > arg0.getY() || (j == 7 && arg0.getY() < 520)) {
												
												int minboxX = (i*50+100), maxboxX = (i*50+150);
												int minboxY = (j*50+100), maxboxY = (j*50+150);
												if (j == 7) maxboxY += 20;
												if (i == 7) maxboxX += 20;
												if(arg0.getX() < maxboxX && arg0.getX() > minboxX && arg0.getY() < maxboxY && arg0.getY() > minboxY) {//This will check if its in the area for the box
													
													int distanceFromTop = (arg0.getY()%50)-2;
													int distanceFromLeft = (arg0.getX()%50)-2;
													int distanceFromBottom = 50 - (arg0.getY()%50)-2;
													int distanceFromRight = 50 - (arg0.getX()%50)-2;
													
													if(arg0.getX() > 500) distanceFromTop = distanceFromLeft = distanceFromBottom = 100;
													if(arg0.getY() > 500) distanceFromTop = distanceFromLeft = distanceFromRight = 100;
													
													System.out.println("Distance from top: " + distanceFromTop);
													System.out.println("Distance from left: " + distanceFromLeft);
													System.out.println("Distance from bottom: " + distanceFromBottom);
													System.out.println("Distance from rright: " + distanceFromRight);

													if((distanceFromTop<distanceFromLeft && distanceFromTop<distanceFromRight && distanceFromTop<distanceFromBottom) && !boxfield[i][j].top) {
														boxfield[i][j].top = true;
														game.change(minboxX, minboxY+2, minboxX+50, minboxY+2);
														//contentPane.paint(contentPane.getGraphics());
														contentPane.paint(contentPane.getGraphics());
														System.out.println("top being painted!");

														if(j > 0) {
															boxfield[i][j-1].bottom = true;
														}
													
														validturn = true;
														
													}
													if((distanceFromBottom<distanceFromLeft && distanceFromBottom<distanceFromRight && distanceFromBottom<distanceFromTop) && !boxfield[i][j].bottom) {
														boxfield[i][j].bottom = true;
														game.change(minboxX, minboxY+52, minboxX+50, minboxY+52);
														contentPane.paint(contentPane.getGraphics());
														
														System.out.println("bottom being painted!");
														if(j < 7) {
															boxfield[i][j+1].top = true;
														}
														
														validturn = true;

													}
													if((distanceFromLeft<distanceFromTop && distanceFromLeft<distanceFromRight && distanceFromLeft<distanceFromBottom) && !boxfield[i][j].left) {
														boxfield[i][j].left = true;
														game.change(minboxX+2, minboxY, minboxX+2, minboxY+50);
														contentPane.paint(contentPane.getGraphics());
														
														System.out.println("left being painted!");
														if(i > 0) {
															boxfield[i-1][j].right = true;
														}
														
														validturn = true;
														
													}
													if((distanceFromRight<distanceFromLeft && distanceFromRight<distanceFromTop && distanceFromRight<distanceFromBottom) && !boxfield[i][j].right) {
														boxfield[i][j].right = true;
														game.change(minboxX+52, minboxY, minboxX+52, minboxY+50);
														contentPane.paint(contentPane.getGraphics());
														
														System.out.println("right being painted!");
														if(i < 7) {
															boxfield[i+1][j].left = true;
														}
														
														validturn = true;
														
													}
												}
											}
										}
									}
								}
								
								for(int i=0;i < 8;i++) { //These for loops will go through and check if any boxes are valid and then award points and draw a the letter of the person who won it
									for(int j=0;j < 8;j++) {
										if(boxfield[i][j].top && boxfield[i][j].bottom && boxfield[i][j].left && boxfield[i][j].right && boxfield[i][j].owner == '-') {
											
											System.out.println("Player " + name + " just won box " + i + " " + j);
											
											if(turn1){
												
												score1++;
												
												P1score.setText(textField.getText().toUpperCase().charAt(0) + ": " + score1);
												
											}
											else{
												
												score2++;
												
												P2score.setText(textField_1.getText().toUpperCase().charAt(0) + ": " + score2);
												
											}
											
											boxfield[i][j].owner = name.toUpperCase().charAt(0);
											
											game.ownerchange(name.toUpperCase().charAt(0) + "",i*50+100,j*50+100);
											contentPane.paint(contentPane.getGraphics());
											
											validturn = false;
											
										}
									}
								}
								
								if(validturn) turn1 = !turn1;
								
								if(turn1) name = textField.getText();
								else name = textField_1.getText();
								
								turn.setText(name + "'s Turn");
								
								contentPane.paint(contentPane.getGraphics());
								
							}
						});
						
						
						
						
						restartbutton.addActionListener(new ActionListener() {
							public void actionPerformed(ActionEvent arg0) {
								//Put restart code in here
								setBounds(800, 300, 437, 311);//Changes and moves the window to look like the player input
								
								//These will make the text fields and labels show again to allow the user know that they restarted the game
								game.setVisible(false);
								continuebutton.setVisible(true);
								lblNewLabel.setVisible(true);
								lblNewLabel_1.setVisible(true);
								lblNewLabel_2.setVisible(true);
								textField.setVisible(true);
								textField_1.setVisible(true);
								
								//This hides the elements of the game
								P1score.setVisible(false);
								P2score.setVisible(false);
								restartbutton.setVisible(false);
								turn.setVisible(false);
								
								//This empty's out the text fields so they will need to input the names again
								textField.setText("");
								textField_1.setText("");
								
								turn1 = true;
								
								//contentPane.repaint();
								
								//running = false;
								
							}
						});
						
						//running = false;
						
					//}	
					
				}
				//System.out.println("Player 1:" + textField.getText() + "Player 2:" + textField_1.getText());
			}
		});

		System.out.println("Game OVer Loser!");
		

	}
	public JTextField getTextField() {
		return textField;
	}
}